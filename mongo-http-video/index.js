const express = require("express");
const fs = require("fs");
const bodyParser = require('body-parser');
const mongodb = require('mongodb');
const mongoose = require("mongoose");

// const url = 'mongodb://user:password@db:27017';
const url = 'mongodb://localhost:27017';
const app = express()

PORT = 8000

app.use(bodyParser.json());


app.get("/", function (req, res) {
  // res.send(`Test-string: Server is started on PORT ${PORT}`) // test server
  res.sendFile(__dirname + "/index.html");
});

// Sorry about this monstrosity
app.get('/init-video', function (req, res) {

  mongodb.MongoClient.connect(url)
      .then((client) => {
        const database = client.db("videos")
        database.collection("fs-video");
        return database
      })
      .then((database) => {
        console.log("DB CREATED")
        const bucket = new mongodb.GridFSBucket(database)
        const videoUploadStream = bucket.openUploadStream('bigbuck');
        const videoReadStream = fs.createReadStream('../bigbuck.mp4');
        videoReadStream.pipe(videoUploadStream);
      })
      .then(() => res.status(200).send("Done..."))
      .catch((err) => console.log(err))
});

let bucket
let range

app.get("/mongo-video", function (req, res) {
  // const range = req.headers.range;
  // console.log(range)
  //   if (!range) {
  //     res.status(400).send("Requires Range header");
  //   }

  mongodb.MongoClient.connect(url)
    .then((client) => {
      range = req.headers.range;
      console.log(range)
      if (!range) {
        res.status(400).send("Requires Range header");
      }
      const database = client.db("videos")
      database.collection("fs-video");
      return database
    })
    .then((database) => {
      bucket = new mongodb.GridFSBucket(database);
      video = database.collection('fs.files').findOne({})
      return video
    })
      .then((video) => {
      // console.log(video.length) // 63614462
      // =================  Create response headers =============
      const CHUNK_SIZE = 10 ** 6; // 1MB
      const videoSize = Number(video.length);
      // const start = Number(range.replace(/\D/g, ""));  // Not properly working
      const start = 0

      // const end = Math.min(start + CHUNK_SIZE, videoSize - 1);
      const end = videoSize - 1
      console.log(`RANGE START: ${start}, END: ${end}`)
      const contentLength = end - start + 1;
      const headers = {
        "Content-Range": `bytes ${start}-${end}/${videoSize}`,
        "Accept-Ranges": "bytes",
        "Content-Length": contentLength,
        "Content-Type": "video/mp4",
      };
      // ==================  HTTP Status 206 for Partial Content ======
      res.writeHead(206, headers);

      // const bucket = new mongodb.GridFSBucket(video);
      const downloadStream = bucket.openDownloadStreamByName('bigbuck', { start });

      // Finally pipe video to response
      downloadStream.pipe(res);
  })
    .catch((err) => console.log(err))
})

app.listen(PORT, function () {
  console.log(`Listening on port ${PORT}!`);
})
