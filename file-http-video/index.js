const express = require("express");
const app = express();
const fs = require("fs");

app.get("/", function (req, res) {
    // 1- res.send("Test-string: Server is started...")
  res.sendFile(__dirname + "/index.html"); // send back HTML file
});

app.get("/video", function (req, res) {
    // Ensure there is a range given for the video
    const range = req.headers.range;
    console.log(range)
    if (!range) {
        res.status(400).send("Requires Range header");
    }

    // get video stats (about 61MB)
    const videoPath = "../bigbuck.mp4";
    const videoSize = fs.statSync("../bigbuck.mp4").size;


    // Parse Range
    // Example: "bytes=32324-"
    const CHUNK_SIZE = 10 ** 6; // 1MB
    const start = Number(range.replace(/\D/g, "")); // replace any non-digit -> ""
    const end = Math.min(start + CHUNK_SIZE, videoSize - 1);
    // console.log()

    // Create headers
    const contentLength = end - start + 1;
    const headers = {
    "Content-Range": `bytes ${start}-${end}/${videoSize}`,
    "Accept-Ranges": "bytes",
    "Content-Length": contentLength,
    "Content-Type": "video/mp4",
    };

    // HTTP Status 206 for Partial Content
    res.writeHead(206, headers);

    // create video read stream for this particular chunk
    const videoStream = fs.createReadStream(videoPath, { start, end });

    // Stream the video chunk to the client
    videoStream.pipe(res);

})


app.listen(3000, function () {
  console.log("Listening on port 3000!");
});